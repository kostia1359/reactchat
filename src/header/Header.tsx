import React, {Component} from "react";
import PropTypes from 'prop-types';
import {getDayName, getTimeString} from "../heplers/timeHelpers";
import './header.css';

interface IProps {
    name: string,
    usersAmount: number,
    messagesAmount: number,
    lastMessage: number
}


class Header extends Component<IProps, {}> {
    static propTypes = {
        name: PropTypes.string,
        usersAmount: PropTypes.number,
        messagesAmount: PropTypes.number,
        lastMessage: PropTypes.number
    }

    constructor(props: IProps) {
        super(props);
    }

    render() {
        const data = this.props;
        const date=new Date(data.lastMessage);
        const lastMessageDate = `${getDayName(date)} at ${getTimeString(date)}`;
        return (
            <div className={"header"}>
                <div className={"header--chatName"}>{data.name}</div>
                <div className={"header--chatUsers"}>{`${data.usersAmount} participants`}</div>
                <div className={"header--chatMessages"}>{`${data.messagesAmount} messages`}</div>
                <div className={"header--chatLastMessage"}>{lastMessageDate}</div>
            </div>

        );
    }
}

export default Header;
