import React, {Component} from "react";

import logo from '../question.svg'
import './main.css'

class MainHeader extends Component<any, any> {
    constructor(props: any) {
        super(props);
    }

    render() {
        return (
            <header>
                <img src={logo} alt="awesome Logo"/>
                <div>{"This is chat logo"}</div>
            </header>
        );
    }
}

export default MainHeader
