import React, {ChangeEvent, Component} from "react";
import PropTypes from 'prop-types';
import "./messageInput.css"

interface IProps {
    addMessage: Function
}

class MessageInput extends Component<IProps, { message: string }> {
    constructor(props: IProps) {
        super(props);
        this.state = {
            message: ""
        }
        this.onChange.bind(this);
    }

    static propTypes = {
        addMessage: PropTypes.func,
    }

    onChange(event: ChangeEvent<HTMLInputElement>, fieldName: 'message') {
        const value = event.target.value;
        this.setState({
            [fieldName]: value
        })
    }

    onClick() {
        const messageValue = this.state.message;
        if (messageValue.trim().length === 0) return;
        this.props.addMessage(messageValue);
        this.setState({
            message: ""
        })
    }

    render() {
        return (
            <div className={"messageInput"}>
                <input value={this.state.message} className={"messageInputField"}
                       onChange={e => this.onChange(e, 'message')}></input>
                <button className={"messageInputButton"} onClick={() => this.onClick()}>Send</button>
            </div>
        )
    }
}

export default MessageInput
