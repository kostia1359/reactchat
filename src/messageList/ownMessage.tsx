import React, {ChangeEvent, Component} from "react";
import PropTypes from 'prop-types';
import {IMessage} from "../types/message";
import {getTimeString} from "../heplers/timeHelpers";
import "./ownMessage.css"

interface IProps {
    message: IMessage,
    editMessage:Function,
    deleteMessage:Function
}

class OwnMessage extends Component<IProps, {isEditing:Boolean, editMessage:string}> {
    constructor(props: IProps) {
        super(props);
        this.state={
            isEditing:false,
            editMessage:""
        }
    }

    static propTypes = {
        message: PropTypes.object,
        editMessage: PropTypes.func,
        deleteMessage: PropTypes.func,
    }

    onEditClick(messageText:string){
        this.setState({
            isEditing:!this.state.isEditing,
            editMessage:messageText
        })
    }

    onChange(event: ChangeEvent<HTMLInputElement>, fieldName: 'editMessage') {
        const value = event.target.value;
        this.setState({
            [fieldName]: value
        })
    }

    onSaveClick(messageId:string){
        this.props.editMessage(messageId, this.state.editMessage);
        this.setState({
            isEditing:!this.state.isEditing,
            editMessage:""
        })
    }

    render() {
        const message = this.props.message;
        const time = new Date(message.createdAt);
        const timeString = getTimeString(time);

        return (
            <div className={"message ownMessage"}>
                <div className={"messageInfo"}>
                    <div className={"messageDate"}>{timeString}</div>
                    <div className={"messageText"} hidden={!!this.state.isEditing}>{message.text}</div>
                    <input className={"editField"} hidden={!this.state.isEditing}
                           value={this.state.editMessage} onChange={e=>this.onChange(e,'editMessage')}></input>
                    <div className={"messageFooter"}>
                        <div className={"messageFooterObject"}>
                            <button className={"editButton"} hidden={!!this.state.isEditing}
                                    onClick={()=>this.onEditClick(message.text)}>Edit</button>
                            <button className={"saveButton"} hidden={!this.state.isEditing}
                            onClick={()=>this.onSaveClick(message.id)}>Save</button>
                        </div>
                        <div className={"messageFooterObject"}>
                            <button className={"deleteButton"} onClick={()=>this.props.deleteMessage(message.id)}>Delete</button>
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}

export default OwnMessage
