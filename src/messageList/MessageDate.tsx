import React, {Component} from "react";
import PropTypes from 'prop-types';
import {getDayName} from "../heplers/timeHelpers";
import "./messageListDate.css"

interface IProps {
    date: Date
}

class MessageDate extends Component<IProps, { message: string }> {
    constructor(props: IProps) {
        super(props);
    }

    static propTypes = {
        date: PropTypes.object,
    }

    render() {
        return (
            <div className="messageListDate">
                <div className={"messageHR"}>
                    <hr/>
                </div>
                <div className={"messageDateBig"}>
                    {getDayName(this.props.date)}
                </div>
                <div className={"messageHR"}>
                    <hr/>
                </div>
            </div>
        )
    }
}

export default MessageDate
