import React, {Component} from "react";
import PropTypes from 'prop-types';
import {IMessage} from "../types/message";
import ForeignMessage from "./foreignMessage";
import OwnMessage from "./ownMessage";
import "./messageList.css"
import {MILLISECONDS_IN_DAY} from "../heplers/constants";
import MessageDate from "./MessageDate";

interface IProps {
    messages: Array<IMessage>,
    editMessage: Function,
    deleteMessage: Function,
}

class MessageList extends Component<IProps, any> {
    constructor(props: IProps) {
        super(props);
    }

    static propTypes = {
        messages: PropTypes.array,
        editMessage: PropTypes.func,
        deleteMessage: PropTypes.func,
    }

    generateDate(message1: IMessage, message2?: IMessage) {
        const dateMessage1 = new Date(message1.createdAt)
        if (message2 === undefined) {
            return (
                <MessageDate date={dateMessage1}></MessageDate>
            )
        }
        const dateMessage2 = new Date(message2.createdAt);

        if (dateMessage2.getTime() - dateMessage1.getTime() > MILLISECONDS_IN_DAY * 2
            || dateMessage1.getDate() !== dateMessage2.getDate()) {
            return (
                <MessageDate date={dateMessage1}></MessageDate>
            )
        }

    }

    getMessage=(message: IMessage, index: number)=> {
        if (message.userId === "0") {
            return <div key={index}>
                {this.generateDate(this.props.messages[index], this.props.messages[index-1])}
                <OwnMessage message={message} editMessage={this.props.editMessage}
                            deleteMessage={this.props.deleteMessage}></OwnMessage>
            </div>
        }
        return (
            <div key={index}>
                {this.generateDate(this.props.messages[index], this.props.messages[index-1])}
                <ForeignMessage message={message}></ForeignMessage>
            </div>
        )
    }

    render() {
        return (
            <div className={"messagesList"}>
                {this.props.messages.map(this.getMessage)}
            </div>
        );
    }
}

export default MessageList
