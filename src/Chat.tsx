import React, {Component} from "react";
import Header from "./header/Header";
import {IMessage} from "./types/message";
import MessageList from "./messageList/messageList";
import MessageInput from "./messageInput/messageInput";
import source from './source.gif';


interface IState {
    messages: Array<IMessage>,
    isDataLoaded: Boolean,
    messagesAmount: number,
    lastMessage: number,
    userAmount: number
}

async function getData(): Promise<IState> {
    const userIDs = new Set<string>();
    const messages = await fetch('https://edikdolynskyi.github.io/react_sources/messages.json').then(res => res.json())
        .then((messages: Array<IMessage>) =>
            messages.sort((message1, message2) => {
                const date1 = new Date(message1.createdAt);
                const date2 = new Date(message2.createdAt);

                return date1.getTime() - date2.getTime();
            })
        );
    messages.forEach(message => userIDs.add(message.userId));


    const lastMessage = new Date(messages[messages.length - 1].createdAt).getTime();
    return {
        messages,
        messagesAmount: messages.length,
        lastMessage,
        isDataLoaded: true,
        userAmount: userIDs.size + 1
    }
}

class Chat extends Component<any, IState> {
    constructor(props: any) {
        super(props);
        this.state = {
            userAmount: 0,
            messages: [],
            messagesAmount: 0,
            lastMessage: 0,
            isDataLoaded: false,
        }
    }

    async componentDidMount() {
        const {messages, messagesAmount, lastMessage, isDataLoaded, userAmount} = await getData();

        this.setState({
            messages,
            messagesAmount,
            lastMessage,
            isDataLoaded,
            userAmount
        })

    }

    render() {
        if (!this.state.isDataLoaded) {
            return (
                <img className={"preloader"} src={source} alt="preloader"/>
            )
        }
        const headerData = {
            name: "chat",
            usersAmount: this.state.userAmount,
            messagesAmount: this.state.messagesAmount,
            lastMessage: this.state.lastMessage
        }

        const editMessage = (messageID: string, text: string) => {
            const messages = this.state.messages.map(message => {
                if (message.id === messageID) {
                    message.text = text;
                    message.editedAt = new Date().toISOString();
                }
                return message;
            });
            this.setState({
                messages
            })
        }

        const deleteMessage = (messageID: string) => {
            const messages = this.state.messages.filter(message => message.id !== messageID);
            this.setState({
                messages,
                messagesAmount: this.state.messagesAmount - 1,
                lastMessage: new Date(messages[messages.length - 1].createdAt).getTime()
            })
        }

        const addMessage = (text: string) => {
            const message: IMessage = {
                user: "you",
                userId: "0",
                id: this.state.messagesAmount.toString(),
                editedAt: "",
                createdAt: new Date().toISOString(),
                avatar: "",
                text,
            }
            const messages = this.state.messages.slice();
            messages.push(message);
            this.setState({
                messages,
                messagesAmount: this.state.messagesAmount + 1,
                lastMessage: Date.now()
            })
        }

        const messagesData = {
            messages: this.state.messages,
            deleteMessage,
            editMessage
        }

        return (
            <div>
                <Header name={headerData.name} usersAmount={headerData.usersAmount}
                        messagesAmount={headerData.messagesAmount} lastMessage={headerData.lastMessage}></Header>
                <MessageList messages={messagesData.messages} editMessage={editMessage}
                             deleteMessage={deleteMessage}></MessageList>
                <MessageInput addMessage={addMessage}></MessageInput>
            </div>
        );
    }
}

export default Chat;
